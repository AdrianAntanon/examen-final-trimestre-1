package Arrays;

public class Arrays_4 {
    public static void main(String[] args) {

        int [] array = {1, 2, 3, 4, 5, 6, 7, 8, 9, 10};
        int i, suma=0;

        for (i=0;i<array.length;i++){
            if (i > 0) {
                if (i%3==0){
                    suma = suma + array[i];
                }
            }

        }

        System.out.println("La suma de las posiciones múltiplos de 3 es " + suma);
    }
}
