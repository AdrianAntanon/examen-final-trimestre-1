package Arrays;

import java.util.Scanner;

public class Arrays_1 {
    public static void main(String[] args) {

        Scanner lector = new Scanner(System.in);

        int[] array = new int[10];
        int i, contador = 0;

        for (i=0;i<array.length;i++){
            array[i]=array[0] + i;
        }

        System.out.println("Introduce un número para buscar si está dentro del array");
        while (!lector.hasNextInt()){
            System.out.println("NOPE, AGAIN");
            lector.next();
        }
        int num = lector.nextInt();

        for (i=0;i<array.length;i++){

            if (num == array[i]){
                contador++;
            }
        }

        if (contador >= 1){
            System.out.println("El número " + num + " sí que aparece dentro del array");
        }else{
            System.out.println("El número " + num + " no aparece dentro del array");
        }




    }
}
