package Arrays;

import java.util.Scanner;

public class Arrays_3 {
    public static void main(String[] args) {
        Scanner lector = new Scanner(System.in);

        int [] array = {1, 2, 3, 4, 5, 6, 7, 8, 9, 10};
        int mayor=0, menor=0, igual=0, i;

        System.out.println("Introduce un número entero, por favor");
        while (!lector.hasNextInt()){
            System.out.println("NOPE, AGAIN");
            lector.next();
        }
        int num = lector.nextInt();

        for (i=0;i<array.length;i++){

            if (num == array[i]){
                igual++;
            }else if (num < array[i]){
                menor++;
            }else{
                mayor++;
            }

        }

        System.out.println("El número " + num + " es más pequeño que " + menor + " números, mayor que " + mayor + " números e igual a " + igual + " números");
    }
}
