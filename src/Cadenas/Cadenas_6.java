package Cadenas;

import java.util.Scanner;

public class Cadenas_6 {
    public static void main(String[] args) {

        Scanner lector = new Scanner(System.in);

        System.out.println("Introduce una frase, por favor");
        String cadena = lector.nextLine().toLowerCase();

        char V = 'v', busqueda;

        int i, contador=0, inicio, fin;
        inicio = cadena.indexOf('v');
        fin = cadena.lastIndexOf('v');

        for (i=0;i<cadena.length();i++){
            busqueda = cadena.charAt(i);
            if (busqueda == V){
                contador++;
            }
        }

        switch (contador){
            case 0: System.out.println("En el texto no figura ninguna letra --> V <--");
                break;
            case 1: System.out.println("En el texto figura una letra --> V <--");
                break;
            default:
                System.out.println("Hay más de una --> V <--, en concreto aparece "+ contador + " veces y abarca lo siguiente partiendo desde la primera vez hasta la última que aparece \n" +
                        cadena.substring(inicio, fin+1));
        }
    }
}
