package Cadenas;

public class Cadenas_1 {
    public static final String frase = "La casa de José es muy grande";
    public static void main(String[] args) {

        System.out.println("Si usamos la función LENGTH nos dará el número de posiciones que tiene un texto, por ejemplo la frase que he puesto de prueba tiene las siguientes posiciones --> " +frase.length());

        int i=0, letras=0;

        for(i=0;i<frase.length();i++){
            if(!(frase.charAt(i) == ' ')){
                letras++;
            }
        }

        System.out.println("Pero si usamos la función charAt para decirle que cuente todos los carácteres menos los espacios tenemos un resultado diferente, con la frase de prueba es --> " + letras);
    }
}
