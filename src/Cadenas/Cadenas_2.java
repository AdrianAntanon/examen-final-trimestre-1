package Cadenas;

import java.util.Scanner;

public class Cadenas_2 {
    public static void main(String[] args) {

        Scanner lector = new Scanner (System.in);

        int i, j;
        String [] frases = new String[5];

        for (i=0;i<frases.length;i++){
            System.out.println("Introduce la palabra " + (i+1) + ", por favor");
            frases[i] = lector.next();
            lector.nextLine();
        }

        for (i=0;i<frases.length;i++){

            if (frases[i].length()%2==0){
                System.out.print(frases[i].substring(0, (frases[i].length()/2)));
            }else{
                System.out.print(frases[i].substring(0, (frases[i].length()/2)+1));
            }
            System.out.println("");
        }

    }
}
