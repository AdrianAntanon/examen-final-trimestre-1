package Cadenas;

import java.util.Scanner;

public class Cadenas_4 {
    public static void main(String[] args) {

        Scanner lector = new Scanner (System.in);


        System.out.println("Introduce una frase, por favor");
        String frase = lector.nextLine();

        String [] palabras = frase.split(" ");
        int i;

        for(i=0;i<palabras.length;i++){
            if (i%2==0){
                System.out.println(palabras[i] + " " + i);
            }
        }
        lector.close();
    }
}
