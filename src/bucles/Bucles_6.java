package bucles;

import java.util.Scanner;

public class Bucles_6 {
    public static void main(String[] args) {
        Scanner lector = new Scanner (System.in);

        int num, i=0, suma=0;

        System.out.println("Introduce un número entero positivo superior a 1, por favor");

        while (!lector.hasNextInt()){
            System.out.println("Vuelve a introducir el número");
            lector.next();
        }

        num = lector.nextInt();

        while (num < 2){
            System.out.println("El número debe ser igual o superior a 2");
            while (!lector.hasNextInt()){
                System.out.println("Vuelve a introducir el número");
                lector.next();
            }
            num = lector.nextInt();
        }

        System.out.print("La serie de números que hacen falta sumar sin llegar a pasarse o ser igual a " + num + " son:\n" +
                "[ ");
        while ((suma+i) < num){
            suma = suma + i;
            System.out.print(i + " ");
            i++;
        }
        System.out.println("] = " + suma);
    }
}
