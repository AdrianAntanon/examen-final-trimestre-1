package bucles;

public class Bucles_8 {
    public static void main(String[] args) {

        // Falta hacer que la altura se introduzca por un Scanner pero me da pereza, aún así ya sabes cómo funciona todo esto

        int altura = 5, i, j;

        for (i=0;i<altura;i++){
            System.out.print("a");
            for (j=0;j<i;j++){
                System.out.print("a");
            }
            System.out.println("");
        }
    }
}
