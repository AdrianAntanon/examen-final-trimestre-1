package bucles;

import java.util.Scanner;

public class Bucles_1 {
    public static void main(String[] args) {

        Scanner lector = new Scanner(System.in);

        int [] array = new int [2];
        int i;

        for (i=0;i<array.length;i++){
            System.out.println("Introduce el número " + (i+1) + ", por favor");
            while (!lector.hasNextInt()){
                System.out.println("Eso no es un número entero, vuelve a introducirlo, por favor");
                lector.next();
            }
            array[i]=lector.nextInt();
            if (i>0){
                while (array[0]>=array[1]){
                    System.out.println("El segundo número no puede ser inferior o igual al primero, vuelve a introducirlo, por favor");
                    while (!lector.hasNextInt()){
                        System.out.println("Eso no es un número entero, vuelve a introducirlo, por favor");
                        lector.next();
                    }
                    array[i]=lector.nextInt();
                }
            }

        }
        System.out.println("El orden de los valores introducidos en orden creciente sería el siguiente: ");
        System.out.print("{ ");
        for (int lista: array){
            System.out.print(lista + " ");
        }
        System.out.println("}");

    }
}
