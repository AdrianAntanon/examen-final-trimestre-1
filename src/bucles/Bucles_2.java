package bucles;

import java.util.Scanner;

public class Bucles_2 {
    public static void main(String[] args) {
        Scanner lector = new Scanner(System.in);

        int [] array = new int [2];
        int i, suma=0;

        for (i=0;i<array.length;i++){
            System.out.println("Introduce el número " + (i+1) + ", por favor");
            while (!lector.hasNextInt()){
                System.out.println("Eso no es un número entero, vuelve a introducirlo, por favor");
                lector.next();
            }
            array[i]=lector.nextInt();
            if (i>0){
                while (array[0]>=array[1]){
                    System.out.println("El segundo número no puede ser inferior o igual al primero, vuelve a introducirlo, por favor");
                    while (!lector.hasNextInt()){
                        System.out.println("Eso no es un número entero, vuelve a introducirlo, por favor");
                        lector.next();
                    }
                    array[i]=lector.nextInt();

                }
            }
            suma = array[0] + array[1];
        }

        System.out.println("La suma entre el primer número " + array[0] + " y el segundo número " + array[1] + " es de " + suma);
    }
}
