package bucles;

import java.util.Scanner;

public class Bucles_4 {
    public static void main(String[] args) {
        Scanner lector = new Scanner(System.in);

        int [] numero = new int [1];
        int i, divisor = 0;

        for (i=0;i<numero.length;i++){
            System.out.println("Introduce el número, por favor");
            while (!lector.hasNextInt()){
                System.out.println("Eso no es un número entero, vuelve a introducirlo, por favor");
                lector.next();
            }
            numero[i]=lector.nextInt();
            while(numero[i]<1){
                System.out.println("El número introducido deberá ser mayor que 0, vuelva a introducirlo, por favor");
                while (!lector.hasNextInt()){
                    System.out.println("Eso no es un número entero, vuelve a introducirlo, por favor");
                    lector.next();
                }
                numero[i]=lector.nextInt();
            }

        }
        System.out.print("El divisor más grande y diferente de él mismo es ");
        for (i=1;i<numero[0];i++){
            if (numero[0]%i==0){
                divisor = i;
            }
        }
        System.out.println(" " + divisor);
    }
}
