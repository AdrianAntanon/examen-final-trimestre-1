package bucles;

import java.util.Scanner;

public class Bucles_5 {
    public static void main(String[] args) {
        Scanner lector = new Scanner (System.in);

        int i=0, num, copiaNum;

        System.out.println("Introduce un número, por favor");
        while (!lector.hasNextInt()){
            System.out.println("Otra vez");
            lector.next();
        }
        num = lector.nextInt();
        copiaNum = num;

        while (copiaNum!=0){
            i++;
            copiaNum=copiaNum/10;
        }

        if (num == 0){
            System.out.println("El número introducido es 0 y tiene 1 cifra");
        }else{
            System.out.println("El número introducido es " + num + " y tiene " + i + " cifra/s");
        }



    }
}
