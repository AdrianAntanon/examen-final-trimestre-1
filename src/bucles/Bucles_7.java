package bucles;

import java.util.Scanner;

public class Bucles_7 {
    public static void main(String[] args) {

        Scanner lector = new Scanner(System.in);

        int [] array = new int[2];
        int i=0;

        for (i=0;i<array.length;i++){
            System.out.println("Introduce el número " + (i+1) + ", por favor");
            while (!lector.hasNextInt()){
                System.out.println("Vuelve a introducirlo");
                lector.next();
            }
            array[i]=lector.nextInt();
        }

        while (array[0] >= array[1]){
            System.out.println("El segundo número nunca podrá ser igual o inferior al primero, vuelve a introducirlo");
            while (!lector.hasNextInt()){
                System.out.println("Vuelve a introducirlo");
                lector.next();
            }
            array[1]=lector.nextInt();
        }
        System.out.print("La lista de números pares entre " + array[0] + " y " + array[1] + " en orden decreciente es la siguiente: \n" +
                "[ ");
        for (i=(array[1]-1);i>array[0];i--){

            if (i%2==0){
                System.out.print(i + " ");
            }

        }
        System.out.println("]");

        lector.close();
    }
}
