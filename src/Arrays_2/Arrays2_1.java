package Arrays_2;

import java.util.Scanner;

public class Arrays2_1 {
    public static void main(String[] args) {

        Scanner lector = new Scanner(System.in);
        System.out.println("Introduce una frase, por favor");

        String frase = lector.nextLine().toLowerCase();


        char [] array = frase.toCharArray();

        int i, contador = 0;
        char letra = 'a', acento = 'á';

        for (i=0;i<array.length;i++){
            if (array[i] == letra || array[i] == acento){
                contador++;
            }
        }

        if (contador > 0){
            System.out.println("La letra -> a <- aparece en " + contador + " ocasión/es");
        }else {
            System.out.println("No aparece la letra -> a <-");
        }
    }
}
