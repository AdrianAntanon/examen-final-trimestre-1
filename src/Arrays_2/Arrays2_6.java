package Arrays_2;

import java.util.Scanner;

public class Arrays2_6 {
    public static void main(String[] args) {
        Scanner lector = new Scanner(System.in);

        int i;
        int [] array = new int[20];

        // El primer for sería para obviar los que introduzca mal, por lo que ese array valdrá 0 y pasará al siguiente

        for (i=0;i<array.length;i++){
            System.out.println("Introduce el valor " +(i+1) + ", por favor");
            if(lector.hasNextInt()){
                array[i] = lector.nextInt();
            }
            lector.nextLine();
        }

        System.out.println("El array quedaría de la siguiente forma");
        for (int lista: array){
            System.out.print(lista + " ");
        }

        // El segundo for es igual que el primero solo que forzando al usuario a introducir un número que pueda ser guardado dentro del array

        System.out.println("Introduce los valores del segundo array");
        for (i=0;i<array.length;i++){
            System.out.println("Introduce el valor " +(i+1) + ", por favor");
            while (!lector.hasNextInt()){
                System.out.println("No has introducido un número entero, AGAIN");
                lector.next();
            }
            array[i] = lector.nextInt();
        }
        System.out.println("El segundo array quedaría de la siguiente forma");
        for (int lista: array){
            System.out.print(lista + " ");
        }
    }
}
