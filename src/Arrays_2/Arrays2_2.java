package Arrays_2;

public class Arrays2_2 {
    public static void main(String[] args) {

        int array1[] = {1, 12, 4, 5, 7, 3, 2, 9, 6, 7}, array2[] = {1, 13, 4, 5, 7, 3, 2, 8, 5, 7};

        int num = 6, i, contador = 0, contador2 = 0;

        for (i=0;i<array1.length;i++){
            if (array1[i] == num){
                contador++;
            }
            if (array2[i] == num){
                contador2++;
            }
        }

        if (contador > 0){
            System.out.println("El número " + num + " sí que aparece dentro del array1");
        }else{
            System.out.println("No aparece el número 6 dentro del array1");
        }

        if (contador2 > 0){
            System.out.println("El número " + num + " sí que aparece dentro del array2");
        }else{
            System.out.println("El número 6 no aparece dentro del array2");
        }

    }
}
