package Arrays_2;

public class Arrays2_4 {
    public static void main(String[] args) {

        int [] array = {1, 2, 3, 4, 5, 6, 7, 8}, array2 = {9, 10, 11, 12, 13, 14, 15, 16};

        int [] suma = new int[16];

        int i, j=0, x=0;

        for (i=0;i<suma.length;i++){

            if (i%2==0){
                suma[i]=array[j];
                j++;
            }else {
                suma[i] = array2[x];
                x++;
            }
        }
        System.out.print("Primer array\n" +
                "[ ");
        for (int lista: array){
            System.out.print(lista + " ");
        }
        System.out.print("]\n" +
                "Segundo array\n" +
                "[ ");
        for (int lista: array2){
            System.out.print(lista + " ");
        }
        System.out.print("]\n" +
                "Combinación del primer y segundo array\n" +
                "[ ");
        for (int lista: suma){
            System.out.print(lista + " ");
        }
        System.out.println("]");
    }
}
