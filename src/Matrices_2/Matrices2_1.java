package Matrices_2;

public class Matrices2_1 {
    public static void main(String[] args) {

        int i, j, contador=0;
        int [][] matriz = new int[10][10];

        for (i=0;i<matriz.length;i++){
            System.out.print("{ ");
            for (j=0;j<matriz[i].length;j++){
                contador++;
                matriz[i][j] = contador;
                System.out.print(matriz[i][j] + " ");
            }
            System.out.println("}");
        }
    }
}
