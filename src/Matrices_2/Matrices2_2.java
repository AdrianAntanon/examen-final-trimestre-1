package Matrices_2;

import java.util.Scanner;

public class Matrices2_2 {
    public static void main(String[] args) {
        Scanner lector = new Scanner (System.in);

        String frase = "";
        char [][] palabras = new char[5][100];
        char letra;
        int i, j, contador = 0;
        int [] array = new int[5];
        String [] frases = new String[5];

        for (i=0;i<palabras.length;i++){
            System.out.println("Introduce la frase " + (i+1)+", por favor");
            frase = lector.nextLine();
            contador=0;
            frases[i]=frase;
            for (j=0;j<frase.length();j++){
                letra = frase.charAt(j);
                palabras[i][j] = letra;
                if (palabras[i][j] != ' '){
                    contador++;
                }
            }
            array[i] = contador;
        }

        for (i=0;i<array.length;i++){
            System.out.println("La frase --> " + frases[i] + " <-- tiene " + array[i] + " letras.");

        }
    }
}
