package Matrices_2;

import java.util.Scanner;

public class Matrices2_6 {
    public static void main(String[] args) {
        Scanner lector = new Scanner(System.in);

        int [][] mediana = new int[3][3];
        int [][] comparativa = new int[3][3];
        int i, j, contador=0, suma=0, media=0;

        System.out.println("Se necesita introducir los valores de una matriz para calcular la mediana, los numeros deben ser enteros, por favor");
        for (i=0;i<mediana.length;i++){
            for (j=0;j<mediana[i].length;j++){
                System.out.println("Introduzca el numero " + (i+1) + "-" + (j+1) + ", por favor");
                while(!lector.hasNextInt()){
                    System.out.println("Eso no es un número entero, vuelva a introducirlo, por favor");
                    lector.next();
                }
                mediana[i][j]=lector.nextInt();
            }
        }

        lector.close();

        System.out.println("La matriz quedaría de la siguiente forma: ");

        for (i=0;i<mediana.length;i++) {
            System.out.print("{ ");
            for (j=0;j<mediana[i].length;j++) {
                contador++;
                suma=mediana[i][j]+suma;
                media=suma/contador;
                System.out.print(mediana[i][j] + " ");
            }
            System.out.println("}");
        }

        System.out.println("\n" +
                "La mediana de los valores de la matriz es: " + media + "\n" +
                "\n" +
                "Si hacemos otra matriz con la comparativa según si los valores son más grandes ( = 1), más pequeños ( = 0) o iguales ( = 2) a la media sería lo siguiente: ");

        for (i=0;i<comparativa.length;i++) {
            System.out.print("{ ");
            for (j=0;j<comparativa[i].length;j++){
                if (mediana[i][j] < media){
                    comparativa[i][j] = 0;
                }else if(mediana[i][j] > media){
                    comparativa[i][j] = 1;
                }else{
                    comparativa[i][j] = 2;
                }
                System.out.print(comparativa[i][j] + " ");
            }
            System.out.println("}");

        }
    }
}
