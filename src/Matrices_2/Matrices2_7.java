package Matrices_2;

public class Matrices2_7 {
    public static void main(String[] args) {

        int [][] primera = {
                {1, 2, 3},
                {4, 5, 6},
                {7, 8, 9}
        };
        int[][] segunda = {
                {10, 20, 30},
                {40, 50, 60},
                {70, 80, 90}
        };
        int [][] suma = new int[3][3];
        int i, j;

        System.out.println("La primera matriz es:");

        for (i=0;i<suma.length;i++){
            System.out.print("{ ");
            for (j=0;j<suma[i].length;j++){
                System.out.print(primera[i][j] + " ");
            }
            System.out.println("}");
        }

        System.out.println("\n" +
                "La segunda matriz es: ");

        for (i=0;i<suma.length;i++){
            System.out.print("{ ");
            for (j=0;j<suma[i].length;j++){
                System.out.print(segunda[i][j] + " ");
            }
            System.out.println("}");
        }

        System.out.println("\n" +
                "La matriz formada por la suma de los valores de ambas sería la siguiente:");

        for (i=0;i<suma.length;i++){
            System.out.print("{ ");
            for (j=0;j<suma[i].length;j++){
                suma[i][j] = primera[i][j] + segunda[i][j];
                System.out.print(suma[i][j] + " ");
            }
            System.out.println("}");
        }

    }
}
