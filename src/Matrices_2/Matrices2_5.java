package Matrices_2;

import java.util.Scanner;

public class Matrices2_5 {
    public static void main(String[] args) {

        Scanner lector = new Scanner(System.in);

        System.out.println("Introduzca la frase, por favor");

        String frase=lector.nextLine().toLowerCase();
        String [] palabras = frase.split(" ");
        String [][] matriz = new String[6][100];
        char [] letras = {'a','e','i','o','u'};
        int i, j, x, contador=0;
        lector.close();

        for (i=0;i<matriz.length;i++) {
            if (i == 0) {
                System.out.print("Frase original es: ");
            } else {
                System.out.print("Frase v." + (i + 1) + " con todas las vocales cambiadas por la letra " + letras[contador] + ", que sería: ");
            }
            for (j=0;j<palabras.length;j++) {
                matriz[i][j] = palabras[j];
                if (i > 0) {
                    for (x=0;x<letras.length;x++) {
                        matriz[i][j] = matriz[i][j].replace(letras[x], letras[contador]);
                    }
                }
                System.out.print(matriz[i][j] + " ");
            }
            if (i > 0) {
                contador++;
            }
            System.out.println("");


        }

    }
}
