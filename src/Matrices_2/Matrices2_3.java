package Matrices_2;

public class Matrices2_3 {
    public static void main(String[] args) {

        int i=0, j=0;
        int[][] notas = new int[10][5];
        String[] nombre = {"Juan", "Carlos", "Adrian", "Alex", "Javier", "Marta", "Jessi", "Miguel", "Mar", "Vero"};

        System.out.print("Nombre del alumno  ");

        for (i=0;i<5;i++){
            System.out.print("Asignatura " + (i+1) + "  ");
        }
        System.out.println("");

        for (i=0;i<notas.length;i++){

            System.out.print(nombre[i]);

            if(nombre[i].length() == 4){
                System.out.print("  ");
            }else if(nombre[i].length() == 3){
                System.out.print("   ");
            }else if(nombre[i].length() == 5){
                System.out.print(" ");
            }
            System.out.print("                  ");

            for (j=0;j<notas[i].length;j++){

                notas[i][j]=(int)(Math.random()*11);

                if (notas[i][j]==10){
                    System.out.print(notas[i][j] + "            ");
                }else{
                    System.out.print(notas[i][j] + "             ");
                }

            }
            System.out.println("");
        }


    }
}
