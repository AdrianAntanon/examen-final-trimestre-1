package Matrices_2;

import java.util.Scanner;

public class Matrices2_8 {
    public static void main(String[] args) {

        Scanner lector = new Scanner(System.in);

        int [][] matriz = new int[3][3];
        int [][] suma = new int[4][3];
        int i, j, x;

        for (i=0;i<matriz.length;i++){
            for (j=0;j<matriz[i].length;j++){
                System.out.println("Introduce un número entero, que correspondará al " + (i+1)+"-"+(j+1));
                while (!lector.hasNextInt()){
                    System.out.println("MAL");
                    lector.next();
                }
                matriz[i][j] = lector.nextInt();
            }
        }

        System.out.println("Si hacemos que la última fila sea la suma de las tres primeras obtenemos lo siguiente: ");

        for (i=0;i<suma.length;i++){
            System.out.print("Fila " + (i+1) + "- ");
            for (j=0;j<suma[i].length;j++){
                if (i<3){
                    suma[i][j] = matriz[i][j];
                }else{
                    for (x=0;x<suma[i].length;x++){
                        suma[i][j] = matriz[x][j] + suma[i][j];
                    }
                }
                System.out.print(suma[i][j] + " ");
            }
            System.out.println(" ");
        }


    }
}
