package condicionales_2;

import java.util.Scanner;

public class Condicionales2_4 {
    public static void main(String[] args) {

        Scanner lector = new Scanner (System.in);

        System.out.println("Introduce un año para saber si es bisiesto o no");
        while (!lector.hasNextInt()){
            System.out.println("Eso no es un año");
            lector.next();
        }
        int año = lector.nextInt();

        if (año%4 == 0 && (año%100) != 0 || año%400 == 0){
            System.out.println("El año es bisiesto");
        }else{
            System.out.println("El año es normal");
        }
    }
}
