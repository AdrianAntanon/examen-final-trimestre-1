package condicionales_2;

import java.util.Scanner;

public class Condicionales2_2 {
    public static final int hora = 30;
    public static final double metro = 0.5;
    public static void main(String[] args) {

        Scanner lector = new Scanner(System.in);

        System.out.println("¿Cuántas horas has trabajado?");
        while (!lector.hasNextInt()){
            System.out.println("Solo se contabilizan las horas de 1 en 1, si has trabajado 1.5 corresponde a 2 horas.\n" +
                    "Vuelve a introducir la cantidad");
            lector.next();

        }
        int htrabajo = lector.nextInt();

        System.out.println("¿Cuántos metros de cable has instalado?");
        while (!lector.hasNextDouble()){
            System.out.println("Eso no son metros de cables, vuelve a introducirlo, por favor");
            lector.next();
        }
        double hcable = lector.nextDouble();

        int pBruto = (htrabajo * hora) + (int)(hcable * metro);

        int pIva = (int) (pBruto*1.21);

        System.out.println("El precio bruto por instalar " + hcable + " metros de cable en " + htrabajo + " horas es de " + pBruto + "€ hablando en bruto, si le añadimos IVA sube hasta " + pIva + "€");

    }
}
