package condicionales_2;

import java.util.Scanner;

public class Condicionales2_1 {
    public static void main(String[] args) {

        Scanner lector = new Scanner(System.in);

        System.out.println("Introduce un número entero, por favor");

        while (!lector.hasNextInt()){
            System.out.println("Vuelve a introducir un número entero, por favor");
            lector.next();
        }
        int num = lector.nextInt();

        if (num%2==0){
            System.out.println("El número " + num + " es par");
        }else{
            System.out.println("El número " + num + " es impar");
        }
    }
}
