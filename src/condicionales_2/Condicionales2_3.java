package condicionales_2;

import java.util.Scanner;

public class Condicionales2_3 {
    public static void main(String[] args) {

        Scanner lector = new Scanner (System.in);

        int [] array = new int[3];
        int i;

        for (i=0; i<array.length;i++){
            System.out.println("Introduzca el número entero " + (i+1) + ", por favor");
            while (!lector.hasNextInt()){
                System.out.println("Eso no es un número entero, vuelve a introducirlo, por favor");
                lector.next();
            }
            array[i]=lector.nextInt();
        }

        if (array[0]<array[1] && array[1]<array[2]){
            System.out.println("Números en orden");
        }else{
            System.out.println("Números desordenados");
        }


    }
}
