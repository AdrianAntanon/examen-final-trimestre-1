package Arrays3;

import java.text.DecimalFormat;

public class Arrays3_1 {
    public static final double maxbarra = 70;
    public static final int array=13;
    public static void main(String[] args) {

        double [] vector= new double [array] ;
        int i,j, x;
        int LongitudBarra=0;
        double valorMaxim;
        valorMaxim = vector[0];

        for(i=0;i<vector.length;i++) {
            vector[i]=(Math.random()*1000);
        }


        for(i=0; i<vector.length;i++) {

            if(valorMaxim<vector[i]) {
                valorMaxim = vector[i];
            }
        }

        // DECIMAL FORMAT es una función que fija los números antes o después de la coma, es decir, 1 con el /formato1/ sería convertido a 1,00
        DecimalFormat formato1 = new DecimalFormat("#.00");
        System.out.println("El gráfico quedaría de la siguiente forma: \n"
                + "");

        for(j=0;j<vector.length;j++) {

            LongitudBarra=(int)(maxbarra*(vector[j]/valorMaxim));
            if(vector[j]<9.99) {
                System.out.print(" "+(formato1.format(vector[j])) + "   | " );
            }else if(vector[j]>99.99) {
                System.out.print(" "+(formato1.format(vector[j])) + " | " );
            }else{
                System.out.print(" "+(formato1.format(vector[j])) + "  | " );
            }

            for(x=0;x<LongitudBarra;x++) {

                System.out.print("=");
            }
            System.out.println(" ");
        }
    }
}
