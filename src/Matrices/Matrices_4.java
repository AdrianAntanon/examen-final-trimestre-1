package Matrices;

public class Matrices_4 {
    public static void main(String[] args) {
        int i, j;

        int[][] matriz = {
                {1, 2, 3},
                {4, 5, 6},
                {7, 8, 9}
        };
        int[][] transpuesta = new int[3][3];
        int[][] resta = new int[3][3];

        System.out.println("Matriz normal: ");

        for (i=0;i<matriz.length;i++){
            System.out.print("{ ");
            for (j=0;j<matriz[i].length;j++){
                System.out.print(matriz[i][j] + " ");
            }
            System.out.println("}");
        }

        System.out.println("Matriz transpuesta: ");

        for (i=0;i<matriz.length;i++){
            System.out.print("{ ");
            for (j=0;j<matriz[i].length;j++){
                transpuesta[i][j] = matriz[j][i];
                System.out.print(transpuesta[i][j] + " ");
            }
            System.out.println("}");
        }

        System.out.println("Matriz con la resta de ambas matrices");

        for (i=0;i<matriz.length;i++){
            System.out.print("{ ");
            for (j=0;j<matriz[i].length;j++){
                resta[i][j] = matriz[i][j] - transpuesta[i][j];
                System.out.print(resta[i][j] + " ");
            }
            System.out.println("}");
        }


    }
}
