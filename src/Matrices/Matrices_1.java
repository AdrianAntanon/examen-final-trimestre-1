package Matrices;

public class Matrices_1 {
    public static void main(String[] args) {

        int [][] matriz = {
                {3, 4, 5},
                {6, 7, 8},
                {9, 10, 11}
        };

        int i, j;

        System.out.println("Versión A: mostrar los valores 5, 7 y 9");

        System.out.println(matriz[0][2] + " - " + matriz[1][1] + " - " + matriz[2][0]);

        System.out.println("Versión B: recorrer los valores de la matriz y mostrarlos por filas");
        for (i=0;i<matriz.length;i++){
            System.out.print("{ ");
            for (j=0;j<matriz[i].length;j++){
                System.out.print(matriz[i][j]+" ");
            }
            System.out.println("}");
        }

        System.out.println("Versión C: mostrarlo por columnas en vez de por filas");

        for (i=0;i<matriz.length;i++){
            System.out.print("{ ");
            for (j=0;j<matriz[i].length;j++){
                System.out.print(matriz[j][i] + " ");
            }
            System.out.println("}");
        }

        System.out.println("Versión D: mostrar los valores que están en diagonal");
        System.out.print("{ ");
        for (i=0;i<matriz.length;i++){
            for (j=0;j<matriz[i].length;j++){
                if (i == j){
                    System.out.print(matriz[i][j]+" ");
                }
            }
        }
        System.out.println("}");

    }
}
