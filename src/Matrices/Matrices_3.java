package Matrices;

import java.util.Scanner;

public class Matrices_3 {
    public static void main(String[] args) {
        Scanner lector = new Scanner(System.in);

        int i, j, contador=0;
        int [][] primero = new int[4][3];
        int [][] segundo = {
                {5, 10},
                {15, 20},
                {25, 30},
                {35, 40}
        };
        int [][] tercero =  {
                {1, 2},
                {3, 4},
                {5, 6},
                {7, 8}
        };
        int [][] suma = new int[4][2];

        System.out.println("Matriz 4x3 con la tabla del 5");
        for (i=0;i<primero.length;i++){
            System.out.print("{ ");
            for (j=0;j<primero[i].length;j++){
                contador++;
                primero[i][j] = contador*5;
                System.out.print(primero[i][j] + " ");
            }
            System.out.println("}");
        }
        System.out.println("Primera matriz: ");
        for (i=0;i<segundo.length;i++){
            System.out.print("{ ");
            for (j=0;j<segundo[i].length;j++){
                System.out.print(segundo[i][j] + " ");
            }
            System.out.println("}");
        }
        System.out.println("Segunda matriz: ");
        for (i=0;i<segundo.length;i++){
            System.out.print("{ ");
            for (j=0;j<segundo[i].length;j++){
                System.out.print(tercero[i][j] + " ");
            }
            System.out.println("}");
        }

        System.out.println("Matriz de la suma de la suma de los valores del primero y segundo: ");
        for (i=0;i<segundo.length;i++){
            System.out.print("{ ");
            for (j=0;j<segundo[i].length;j++){
                suma[i][j] = segundo[i][j] + tercero[i][j];
                System.out.print(suma[i][j] + " ");
            }
            System.out.println("}");
        }

    }
}
