package Matrices;

import java.util.Scanner;

public class Matrices_2 {
    public static void main(String[] args) {

        Scanner lector = new Scanner(System.in);

        int [][] matriz = new int[3][3];

        int i, j, max=matriz[0][0], min=99999999;

        for (i=0;i<matriz.length;i++){
            for (j=0;j<matriz[i].length;j++){

                System.out.println("Introduce el valor de " + (i+1) + "-" + (j+1) + ", por favor");

                while (!lector.hasNextInt()){
                    System.out.println("AGAIN");
                    lector.next();
                }

                matriz[i][j] = lector.nextInt();

                while (matriz[i][j] < 1){
                    System.out.println("AGAIN SHIT");
                    while (!lector.hasNextInt()){
                        System.out.println("AGAIN");
                        lector.next();
                    }
                    matriz[i][j] = lector.nextInt();
                }

                if (max < matriz[i][j]){
                    max =  matriz[i][j];
                }

                if (min> matriz[i][j]){
                    min =  matriz[i][j];
                }
            }
        }

        System.out.println("Matriz normal:");
        for (i=0;i<matriz.length;i++){
            System.out.print("{ ");
            for (j=0;j< matriz[i].length;j++){
                System.out.print( matriz[i][j] + " ");
            }
            System.out.println("}");
        }
        System.out.println("Matriz vista al revés: ");
        for (i=matriz.length-1;i>=0;i--){
            System.out.print("{ ");
            for (j=0;j< matriz[i].length;j++){
                System.out.print( matriz[i][j] + " ");
            }
            System.out.println("}");
        }

        System.out.println("Y el valor mínimo es " + min + " y el máximo por su contra es " + max);


    }
}
