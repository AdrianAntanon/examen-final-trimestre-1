package Cadenas_2;

import java.util.Scanner;

public class Cadenas2_8 {
    public static void main(String[] args) {

        Scanner lector = new Scanner (System.in);

        System.out.println("Introduce una frase, por favor");
        String frase = lector.nextLine().toLowerCase();


        if (frase.indexOf("hola") > 0){
            System.out.println("La palabra hola comienza en la posición "+frase.indexOf("hola"));
        }else{
            System.out.println("La palabra hola no aparece");
        }


    }
}
