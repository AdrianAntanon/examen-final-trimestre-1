package Cadenas_2;

public class Cadenas2_2 {
    public static void main(String[] args) {

        String frase = "Orden inverso";
        int i;

        System.out.print("La frase original es " +frase + " y si la ponemos al revés sería ");
        for (i=frase.length();i>0;i--){
            System.out.print(frase.charAt(i-1));
        }
    }
}
