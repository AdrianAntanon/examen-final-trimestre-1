package Cadenas_2;

public class Cadenas2_6 {
    public static void main(String[] args) {
        String palabra1 = "venganza", palabra2 = "arrepentimiento";

        int posicion = palabra1.compareTo(palabra2);

        System.out.println("Ordenadas por orden alfabético sería de la siguiente forma");

        if (posicion<0){
            System.out.println("Primero " + palabra1 + " y segundo " + palabra2);
        }else{
            System.out.println("Primero " + palabra2 + " y segundo " + palabra1);
        }
    }
}
