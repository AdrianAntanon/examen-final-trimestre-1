package condicionales;

import java.util.Scanner;

public class Condicionales_1 {
    public static void main(String[] args) {

        Scanner lector = new Scanner(System.in);

        System.out.println("Introduce un número entero, por favor");
        while(!lector.hasNextInt()){
            System.out.println("Eso no es un número entero, vuelve a introducirlo, por favor");
            lector.next();
        }
        int num = lector.nextInt();

        int invertido = num * -1;

        System.out.println("El número introducido:" + num + " se vería de la siguiente forma con el símbolo invertido: " + invertido);
    }
}
