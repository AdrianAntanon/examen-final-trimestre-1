package condicionales;

import java.util.Scanner;

public class Condicionales_2 {
    public static void main(String[] args) {
        Scanner lector = new Scanner(System.in);

        int [] array = new int [3];

        int i=0, num;

        for (i=0; i<array.length;i++){
            System.out.println("Introduce el número entero " + (i+1) +", por favor");
            while (!lector.hasNextInt()){
                System.out.println("Eso no es un número entero, vuelve a introducirlo, por favor");
                lector.next();
            }
            array[i]=lector.nextInt();
        }

        if (array[1]<array[2] && array[1]>array[0]){
            System.out.println("El número 2 -> " + array[1] + " es inferior al número 3 -> " + array[2] + " pero es superior al número 1 -> " + array[0] );
        }else if(array[1]>=array[2] && array[1]>array[0]){
            System.out.println("El número 2 -> " + array[1] + " es no inferior al número 3 -> " + array[2] + " pero sigue siendo superior al número 1 -> " + array[0] );
        }else{
            System.out.println("Me da pereza programar esto, pero básicamente o los números son iguales, o el número 1 es superior al número 2");
        }



    }
}
