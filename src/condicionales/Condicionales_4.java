package condicionales;

import java.util.Scanner;

public class Condicionales_4 {
    public static void main(String[] args) {
        Scanner lector = new Scanner(System.in);

        int respuesta;

        System.out.println("¿Es 1. Animal, 2. Vegetal o 3. Mineral? (introduce 1, 2 o 3)");

        while (!lector.hasNextInt()){
            System.out.println("Eso no es un número entero, vuelve a introducirlo, por favor");
            lector.next();
        }
        respuesta=lector.nextInt();
        while (respuesta<1 && respuesta>3){
            System.out.println("Solo pueden ser 1, 2 o 3, vuelve a introducirlo, por favor");
            while (!lector.hasNextInt()){
                System.out.println("Eso no es un número entero, vuelve a introducirlo, por favor");
                lector.next();
            }
            respuesta=lector.nextInt();
        }

        if(respuesta == 1) {
            System.out.println("¿Cuántas patas tiene?");
            respuesta=lector.nextInt();
            if(respuesta == 2) {
                System.out.println("¿Es una gallina? (1=sí) (2=no)");
                respuesta=lector.nextInt();
                if(respuesta == 1) {
                    System.out.println("Lo acerté");
                }else if(respuesta == 2) {
                    System.out.println("Es un pato");
                }
            }else if( respuesta == 4) {
                System.out.println("¿Es un perro?(1=sí) (2=no)");
                respuesta=lector.nextInt();
                if(respuesta == 1) {
                    System.out.println("Lo acerté");
                }else if(respuesta == 2) {
                    System.out.println("Es un gato");
                }
            }else{
                System.out.println("Paso de bichos");
            }

        }else if (respuesta == 2) {
            System.out.println("¿Es verde? (1=sí) (2=no)");
            respuesta=lector.nextInt();
            if( respuesta == 1) {
                System.out.println("Es una lechuga");
            }else if( respuesta == 2) {
                System.out.println("Es un tomate");
            }
        }else if( respuesta == 3) {
            System.out.println("Es una piedra, y me quedo tan ancho motherfucker");
        }

        lector.close();


    }
}
