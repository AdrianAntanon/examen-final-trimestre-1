package condicionales;

import java.util.Scanner;

public class Condicionales_3 {
    public static void main(String[] args) {

        Scanner lector = new Scanner(System.in);

        int [] array = new int[2];
        int i, multi, suma;

        for (i=0;i<array.length;i++){
            System.out.println("Introduce un número entero, por favor");
            while (!lector.hasNextInt()){
                System.out.println("Eso no es un número entero, vuelve a introducirlo, por favor");
                lector.next();
            }
            array[i]=lector.nextInt();
        }

        multi = array[0]*array[1];
        suma = array[0]+array[1];

        if (multi == suma){
            System.out.println("El resultado de la multiplicación y suma es el mismo --> " + multi);
        }else if(multi>suma){
            System.out.println("La multiplicación entre ambos valores da " + multi + " y es más grande que la suma, que da " + suma);
        }else{
            System.out.println("La multiplicación entre ambos valores da " + multi + " y es más pequeña que la suma, que da " + suma);
        }


    }
}
