package Arrays4;

import java.util.Scanner;

public class Arrays4_1 {
    public static final int modulo = 23;
    public static void main(String[] args) {

        Scanner lector = new Scanner(System.in);
        char [] letras = {'T', 'R', 'W', 'A', 'G', 'H', 'Y', 'F', 'P', 'D', 'X', 'B', 'N', 'J', 'Z', 'S', 'Q', 'V', 'H', 'L', 'C', 'K', 'E'};
        int DNI=0, i=0;

        while(!lector.hasNextInt()) {
            System.out.println("Eso no es un número de DNI válido, vuelva a introducirlo, por favor");
            lector.next();
        }

        DNI=lector.nextInt();
        while(DNI<10000000 || DNI>99999999) {
            System.out.println("No existe el DNI negativo o que sea 0, tampoco que sea más o menos de 8 dígitos, vuelva a introducirlo, por favor");
            while(!(lector.hasNextInt())) {
                System.out.println("Eso no es un número de DNI válido, vuelva a introducirlo, por favor");
                lector.next();
            }
            DNI=lector.nextInt();
        }
        lector.close();

        for (i=0;i<letras.length;i++){

            if (DNI%modulo == i){
                System.out.println("El número introducido " + DNI + " le corresponde la letra " + letras[i] + "\n" +
                        "Quedaría de la siguiente forma " + DNI + "-"+letras[i]);
            }
        }
    }
}
