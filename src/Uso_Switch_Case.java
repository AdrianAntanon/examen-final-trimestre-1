import java.util.Scanner;

public class Uso_Switch_Case {
    public static void main(String[] args) {
        Scanner lector = new Scanner(System.in);

        int day;
        int prueba, macarron=0;

        System.out.println("Introduce el número del día de la semana del cual quieres obtener información, por favor");

        while (!lector.hasNextInt()){
            System.out.println("Recuerde que el número debe ser en formato entero");
            lector.next();
        }

        day=lector.nextInt();

        while (day<1 || day>7){
            System.out.println("Ese número no es válido, la semana tiene 7 días, vuelve a introducirlo, por favor");
            while (!lector.hasNextInt()){
                System.out.println("Recuerde que el número debe ser en formato entero");
                lector.next();
            }
            day=lector.nextInt();
        }

        String dayType;
        String dayString;

        switch (day)
        {
            case 1:  dayString = "Lunes";
                break;
            case 2:  dayString = "Martes";
                break;
            case 3:  dayString = "Miercoles";
                break;
            case 4:  dayString = "Jueves";
                break;
            case 5:  dayString = "Viernes";
                break;
            case 6:  dayString = "Sabado";
                break;
            case 7:  dayString = "Domingo";
                System.out.println("pregunta de prueba, solo 10 hará que macarrón = 10, el resto de números harán que sea macarrón = 5");
                prueba = lector.nextInt();
                switch (prueba){
                    case 10: macarron = 10;
                        break;
                    default: macarron = 5;
                }
                break;
            default: dayString = "Dia invalido";
        }

        switch (day)
        {
            //multiples cases sin declaraciones break

            case 1:
            case 2:
            case 3:
            case 4:
            case 5:
                dayType = "dia laborable";
                break;
            case 6:
            case 7:
                dayType = "fin de semana";
                break;

            default: dayType= "tipo de dia invalido";
        }

        System.out.println("El "+dayString+" es un "+ dayType + "\n" +
                "macarrón = " + macarron);

    }
}
